package com.marathon.schedule;

import com.marathon.common.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
@Transactional
@EnableScheduling
public class UpdateConfirmationCode {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateConfirmationCode.class);

    @PersistenceContext
    private final EntityManager entityManager;

    @Autowired
    public UpdateConfirmationCode(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Scheduled(cron = "0 */15 * ? * *")
    public void deleteCodeAfterFifteenMin() {
        entityManager.createQuery(Constants.Schedule.DELETE_CODE_CONFIRMATION).executeUpdate();
        LOGGER.info("DELETE CONFIRMATION CODE");
    }
}
