package com.marathon.jms;

import com.marathon.common.util.MailService;
import com.marathon.model.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;

@Component
public class Listener {

    @Autowired
    private MailService mailService;

    @JmsListener(destination = "registration")
    public void registration(final ObjectMessage message) throws JMSException {
        sendEmail(message);
    }

    @JmsListener(destination = "reset-code")
    public void resetCode(final ObjectMessage message) throws JMSException {
        sendEmail(message);
    }

    @JmsListener(destination = "reset-key")
    public void resetKey(final ObjectMessage message) throws JMSException {
        sendEmail(message);
    }

    @JmsListener(destination = "congratulation")
    public void congratulation(final ObjectMessage message) throws JMSException {
        sendEmail(message);
    }


    private void sendEmail(final ObjectMessage jsonMessage) throws JMSException {
        if (jsonMessage != null) {
            Email email = (Email) jsonMessage.getObject();
            mailService.sendEmail(email.getTo(), email.getSubject(), email.getCode(), false, true);
        }
    }
}
