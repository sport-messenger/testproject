package com.marathon.jms;

import com.marathon.model.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class Producer {

    @Autowired
    JmsTemplate jmsTemplate;

    public void sendMessage(final String queueName, final Email email) {
        jmsTemplate.convertAndSend(queueName, email);
    }
}
