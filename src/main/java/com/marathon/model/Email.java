package com.marathon.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Email implements Serializable {

    private String from;
    private String to;
    private String subject;
    private String code;
}
