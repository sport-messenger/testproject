package com.marathon.model;

import com.marathon.common.Constants;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "marathon")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Marathon extends BaseEntity {

    @Column(name = "name", nullable = false)
    private String name;

    @Size(max = Constants.Marathon.MAX_SIZE_FOR_DESCRIPTION_OF_MARATHON)
    @Column(name = "description", nullable = false)
    private String description;

    @ManyToMany(mappedBy = "marathons", fetch = FetchType.LAZY)
    private List<User> users;
}
