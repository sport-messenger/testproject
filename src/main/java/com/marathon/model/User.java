package com.marathon.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.marathon.model.enumeration.Gender;
import com.marathon.model.enumeration.UserStatus;
import com.marathon.common.Constants;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;


@Getter
@Setter
@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class User extends BaseEntity {

    @Size(min = Constants.User.MIN_SIZE_FOR_USER_NAME, max = Constants.User.MAX_SIZE_FOR_USER_NAME)
    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @JsonIgnore
    @Size(min = Constants.User.MIN_SIZE_FOR_PASSWORD, max = Constants.User.MAX_SIZE_FOR_PASSWORD)
    @Column(name = "password", nullable = false)
    private String password;

    @Size(min = Constants.User.MIN_SIZE_FOR_NAME, max = Constants.User.MAX_SIZE_FOR_NAME)
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Size(min = Constants.User.MIN_SIZE_FOR_NAME, max = Constants.User.MAX_SIZE_FOR_NAME)
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Size(min = Constants.User.MIN_SIZE_FOR_EMAIL, max = Constants.User.MAX_SIZE_FOR_EMAIL)
    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "avatar")
    private String avatar;

    @Column(name = "height")
    private double height;

    @Column(name = "weight")
    private double weight;

    @Column(name = "age")
    private Date age;

    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @NotNull
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private UserStatus status;

    @Column(name = "reset_key")
    private Integer resetKey;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
    private List<Role> roles;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_marathons",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "marathon_id", referencedColumnName = "id")})
    private List<Marathon> marathons;
}
