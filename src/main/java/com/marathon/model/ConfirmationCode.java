package com.marathon.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "confirmation_code")
@EqualsAndHashCode(callSuper = false)
public class ConfirmationCode extends BaseEntity {

        @Column(name = "code")
        private int code;

        @OneToOne(cascade = CascadeType.ALL)
        private User user;

        @Column(name = "expired")
        private Date expired;

}
