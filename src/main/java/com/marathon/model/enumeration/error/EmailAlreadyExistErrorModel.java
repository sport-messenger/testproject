package com.marathon.model.enumeration.error;

import com.marathon.common.error.model.ErrorModel;

public class EmailAlreadyExistErrorModel extends ErrorModel {
    public EmailAlreadyExistErrorModel() {
        super("1000.0", "Email already exist!");
    }
}
