package com.marathon.model.enumeration.error;

import com.marathon.common.error.model.ErrorModel;

public class ActivatedCodeErrorModel extends ErrorModel {
    public ActivatedCodeErrorModel() {
        super("1000.0", "Activated code doesn't match");
    }
}
