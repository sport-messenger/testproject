package com.marathon.model.enumeration.error;

import com.marathon.common.error.model.ErrorModel;

public class UpdatePasswordErrorModel extends ErrorModel {
    public UpdatePasswordErrorModel() {
        super("1000.0", "Password mismatch");
    }
}
