package com.marathon.model.enumeration.error;

import com.marathon.common.error.model.ErrorModel;

public class BadCredentialsErrorModel extends ErrorModel {
    public BadCredentialsErrorModel() {
        super("1000.0", "Invalid username or password");
    }
}
