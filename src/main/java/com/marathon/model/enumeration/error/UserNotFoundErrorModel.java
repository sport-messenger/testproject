package com.marathon.model.enumeration.error;

import com.marathon.common.error.model.ErrorModel;

public class UserNotFoundErrorModel extends ErrorModel {
    public UserNotFoundErrorModel() {
        super("1000.0", "User not found");
    }
}
