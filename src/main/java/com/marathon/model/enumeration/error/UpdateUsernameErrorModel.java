package com.marathon.model.enumeration.error;

import com.marathon.common.error.model.ErrorModel;

public class UpdateUsernameErrorModel extends ErrorModel {
    public UpdateUsernameErrorModel() {
        super("1000.0", "Username already exist!");
    }
}
