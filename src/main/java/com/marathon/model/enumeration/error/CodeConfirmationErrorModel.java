package com.marathon.model.enumeration.error;

import com.marathon.common.error.model.ErrorModel;

public class CodeConfirmationErrorModel extends ErrorModel {
    public CodeConfirmationErrorModel() {
        super("1000.0", "Reset key don`t match");
    }
}
