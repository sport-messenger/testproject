package com.marathon.model.enumeration.error;

import com.marathon.common.error.model.ErrorModel;

public class UserAlreadyActivatedErrorModel extends ErrorModel {
    public UserAlreadyActivatedErrorModel() {
        super("1000.0", "User already activated!");
    }
}
