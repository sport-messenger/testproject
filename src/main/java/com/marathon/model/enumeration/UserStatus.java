package com.marathon.model.enumeration;

public enum UserStatus {
    ACTIVE, NOT_ACTIVE, DELETED
}
