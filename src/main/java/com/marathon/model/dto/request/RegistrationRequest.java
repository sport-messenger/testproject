package com.marathon.model.dto.request;

import com.marathon.common.Constants;
import com.marathon.common.annotation.FieldEquals;
import com.marathon.model.BaseEntity;
import com.marathon.model.enumeration.Gender;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@Setter
@FieldEquals(field = "password", equalsTo = "confirmPassword")
public class RegistrationRequest extends BaseEntity {
    private static final String PASSWORD_REGEX = "(?=^.{6,}$)((?=.*\\d)|(?=.*\\W+))(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$";
    private static final String EMAIL_REGEX = "^[-\\w.]+@([A-z0-9][-A-z0-9]+\\.)+[A-z]{2,4}$";

    @NotNull
    @Size(min = Constants.User.MIN_SIZE_FOR_USER_NAME, max = Constants.User.MAX_SIZE_FOR_USER_NAME)
    private String username;

    @NotNull
    @Pattern(regexp = PASSWORD_REGEX)
    @Size(min = Constants.User.MIN_SIZE_FOR_PASSWORD, max = Constants.User.MAX_SIZE_FOR_PASSWORD)
    private String password;

    @NotNull
    @Size(min = Constants.User.MIN_SIZE_FOR_PASSWORD, max = Constants.User.MAX_SIZE_FOR_PASSWORD)
    private String confirmPassword;

    @NotNull
    @Size(min = Constants.User.MIN_SIZE_FOR_NAME, max = Constants.User.MAX_SIZE_FOR_NAME)
    private String firstName;

    @NotNull
    @Size(min = Constants.User.MIN_SIZE_FOR_NAME, max = Constants.User.MAX_SIZE_FOR_NAME)
    private String lastName;

    @NotNull
    @Pattern(regexp = EMAIL_REGEX)
    @Size(min = Constants.User.MIN_SIZE_FOR_EMAIL, max = Constants.User.MAX_SIZE_FOR_EMAIL)
    private String email;

    private String avatar;

    private double height;

    private double weight;

    private Date age;

    private Gender gender;

}
