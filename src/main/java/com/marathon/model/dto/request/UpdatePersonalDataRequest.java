package com.marathon.model.dto.request;

import com.marathon.common.Constants;
import com.marathon.model.enumeration.Gender;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdatePersonalDataRequest {

    @Size(min = Constants.User.MIN_SIZE_FOR_USER_NAME, max = Constants.User.MAX_SIZE_FOR_USER_NAME)
    private String username;

    @Size(min = Constants.User.MIN_SIZE_FOR_NAME, max = Constants.User.MAX_SIZE_FOR_NAME)
    private String firstName;

    @Size(min = Constants.User.MIN_SIZE_FOR_NAME, max = Constants.User.MAX_SIZE_FOR_NAME)
    private String lastName;

    private String avatar;

    private double height;

    private double weight;

    private Date age;

    private Gender gender;
}
