package com.marathon.model.dto.request;

import com.marathon.common.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticationRequest {

    @NotNull
    @Size(min = Constants.User.MIN_SIZE_FOR_USER_NAME, max = Constants.User.MAX_SIZE_FOR_USER_NAME)
    private String username;

    @NotNull
    @Size(min = Constants.User.MIN_SIZE_FOR_PASSWORD, max = Constants.User.MAX_SIZE_FOR_PASSWORD)
    private String password;
}
