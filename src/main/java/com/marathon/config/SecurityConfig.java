package com.marathon.config;

import com.marathon.common.error.ExceptionHandlerFilter;
import com.marathon.security.jwt.JwtConfigurer;
import com.marathon.security.jwt.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    private static final String TRAINER_ENDPOINTS = "/api/v1/trainer/**";
    private static final String LOGIN_ENDPOINT = "/api/v1/auth/login";
    private static final String ACCOUNT_ENDPOINT = "/api/v1/accounts/**";
    private static final String REGISTER_ENDPOINT = "/api/v1/auth/registration";
    private static final String USER_ENDPOINTS = "/api/v1/user/**";
    private static final String RECOVER_PASSWORD = "/api/v1/recover-passwords/**";

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .httpBasic().disable()
                .csrf().disable()
                .addFilterBefore(new ExceptionHandlerFilter(), UsernamePasswordAuthenticationFilter.class)
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(LOGIN_ENDPOINT).permitAll()
                .antMatchers(ACCOUNT_ENDPOINT).permitAll()
                .antMatchers(REGISTER_ENDPOINT).permitAll()
                .antMatchers(RECOVER_PASSWORD).permitAll()
                .antMatchers(TRAINER_ENDPOINTS).hasRole("TRAINER")
                .anyRequest().authenticated()
                .and()
                .apply(new JwtConfigurer(jwtTokenProvider));
    }
}
