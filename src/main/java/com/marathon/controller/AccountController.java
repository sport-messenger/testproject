package com.marathon.controller;

import com.marathon.exeption.UserException;
import com.marathon.model.User;
import com.marathon.model.dto.request.CodeRequest;
import com.marathon.model.dto.response.BasicResponse;
import com.marathon.service.AccountService;
import com.marathon.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping(value = "/api/v1")
public class AccountController {

    @Autowired
    private UserService userService;
    @Autowired
    private AccountService accountService;

    @PostMapping("/accounts")
    public ResponseEntity<?> activateAccount(@RequestBody CodeRequest code, Principal principal) {
        User user = userService.findByUsername(principal.getName());
        return ResponseEntity.ok(accountService.activateAccount(code.getCode(), user));
    }

    @GetMapping("/accounts/check-username")
    public ResponseEntity<BasicResponse> checkUsernameIfExists(@RequestParam("username") String username) {
        try {
            userService.findByUsername(username);
        } catch (UserException e) {
            return ResponseEntity.ok(new BasicResponse(true, "Username is free"));
        }
        return ResponseEntity.badRequest().body(new BasicResponse(false, "Username already exists!"));
    }

    @GetMapping("/accounts/check-email")
    public ResponseEntity<BasicResponse> checkEmailIfExists(@RequestParam("email") String email) {
        try {
            userService.findByEmail(email);
        } catch (UserException e) {
            return ResponseEntity.ok(new BasicResponse(true, "Email is available"));
        }
        return ResponseEntity.badRequest().body(new BasicResponse(false, "Email already exists!"));
    }
}
