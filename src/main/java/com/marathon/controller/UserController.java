package com.marathon.controller;


import com.marathon.model.User;
import com.marathon.model.dto.request.UpdateDataRequest;
import com.marathon.model.dto.request.UpdatePasswordRequest;
import com.marathon.model.dto.request.UpdatePersonalDataRequest;
import com.marathon.model.dto.response.BasicResponse;
import com.marathon.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping(value = "/api/v1")
public class UserController {

    @Autowired
    private UserService userService;

    @PatchMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> updatePersonalData(@RequestBody UpdatePersonalDataRequest request,
                                                   Principal principal) {
        return ResponseEntity.ok(userService.updatePersonalData(principal.getName(), request));
    }

    @GetMapping(value = "/users")
    public ResponseEntity<User> getCurrentUser(Principal principal) {
        return ResponseEntity.ok(userService.findByUsername(principal.getName()));
    }

    @PatchMapping("/users/update-password")
    public ResponseEntity<BasicResponse> updatePasswordWithCurrent(@RequestBody UpdatePasswordRequest request, Principal principal) {
        User user = userService.findByUsername(principal.getName());


        userService.updatePassword(user, request.getCurrentPassword(), request.getPassword(), request.getConfirmPassword());
        return ResponseEntity.ok(new BasicResponse(true, "Password changed"));
    }

    @PatchMapping("/users/update-email")
    public ResponseEntity<BasicResponse> updateEmail(@RequestBody UpdateDataRequest request, Principal principal) {
        User user = userService.findByUsername(principal.getName());

        userService.updateEmail(request.getEmail(), request.getCurrentPassword(), user);
        return ResponseEntity.ok(new BasicResponse(true, "Email changed"));
    }

    @PatchMapping("/users/update-username")
    public ResponseEntity<BasicResponse> updateUsername(@RequestBody UpdateDataRequest request, Principal principal) {
        User user = userService.findByUsername(principal.getName());
        userService.updateUsername(request.getUsername(), user);
        return ResponseEntity.ok(new BasicResponse(true, "Username changed"));
    }
}
