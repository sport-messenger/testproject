package com.marathon.controller;

import com.marathon.exeption.UserException;
import com.marathon.model.User;
import com.marathon.model.dto.request.AuthenticationRequest;
import com.marathon.model.dto.request.RegistrationRequest;
import com.marathon.model.dto.response.AuthenticationResponse;
import com.marathon.model.dto.response.BasicResponse;
import com.marathon.model.enumeration.error.BadCredentialsErrorModel;
import com.marathon.security.jwt.JwtTokenProvider;
import com.marathon.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.text.ParseException;

@RestController
@RequestMapping(value = "/api/v1/auth")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private UserService userService;

    //TODO test on this method (problem with 43)
    @PostMapping("/login")
    public ResponseEntity<?> login(@Valid @RequestBody AuthenticationRequest requestDto) {
        try {
            String username = requestDto.getUsername();
            // throw exception if user has status NOT_ACTIVE (DisabledException)
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, requestDto.getPassword()));
            User user = userService.findByUsername(username);
            String token = jwtTokenProvider.createToken(username, user.getRoles());
            return ResponseEntity.ok(new AuthenticationResponse(token));
        } catch (AuthenticationException e) {
            throw new UserException(HttpStatus.UNPROCESSABLE_ENTITY, new BadCredentialsErrorModel());
        }
    }

    //TODO test on this method (problem with parse age)
    @PostMapping("/registration")
    public ResponseEntity<?> registration(@Valid @RequestBody RegistrationRequest request) throws ParseException {
        User user = userService.findByUsernameOrEmail(request.getUsername(), request.getEmail());
        if (user != null) {
            return ResponseEntity.unprocessableEntity().body(new BasicResponse(false, "Email or username already exist"));
        }
        return ResponseEntity.ok(userService.userRegistration(request));
    }

}
