package com.marathon.controller;

import com.marathon.model.User;
import com.marathon.model.dto.response.BasicResponse;
import com.marathon.service.ConfirmationCodeService;
import com.marathon.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1")
public class ConfirmationCodeController {

    @Autowired
    private UserService userService;
    @Autowired
    private ConfirmationCodeService confirmationCodeService;

    @PutMapping("/confirmation-codes/{userId}")
    public ResponseEntity resetCodeConfirmation(@PathVariable(value = "userId") String userId) {
        User user = userService.findById(Long.valueOf(userId));
        confirmationCodeService.resetCode(user);
        return ResponseEntity.ok(new BasicResponse(true, "Code has been reset"));
    }
}
