package com.marathon.controller;

import com.marathon.model.User;
import com.marathon.model.dto.request.ConfirmationCodeRequest;
import com.marathon.model.dto.request.UpdatePasswordRequest;
import com.marathon.model.dto.response.BasicResponse;
import com.marathon.service.RecoverPasswordService;
import com.marathon.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1")
public class RecoverPasswordController {

    @Autowired
    private RecoverPasswordService recoverPasswordService;
    @Autowired
    private UserService userService;

    @PostMapping("/recover-passwords")
    public ResponseEntity createResetKey(@RequestBody ConfirmationCodeRequest request) {
        User user = userService.findByEmail(request.getEmail());
        recoverPasswordService.createResetKey(user);
        return ResponseEntity.ok(new BasicResponse(true, "Reset code was created"));
    }

    @PostMapping("/recover-passwords/check")
    public ResponseEntity checkResetKey(@RequestBody ConfirmationCodeRequest request) {
        User user = userService.findByEmail(request.getEmail());
        recoverPasswordService.checkResetKey(user, Integer.parseInt(request.getResetKey()));

        return ResponseEntity.ok(new BasicResponse(true, "Reset key confirmed"));
    }

    @PatchMapping("/recover-passwords")
    public ResponseEntity updatePassword(@RequestBody UpdatePasswordRequest request) {
        User user = userService.findByEmail(request.getEmail());

        recoverPasswordService.recoverPassword(user, request.getPassword(), request.getConfirmPassword());
        return ResponseEntity.ok(new BasicResponse(true, "Password changed"));
    }
}
