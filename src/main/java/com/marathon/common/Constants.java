package com.marathon.common;

public class Constants {

    public class User {
        public static final int MIN_SIZE_FOR_USER_NAME = 4;
        public static final int MAX_SIZE_FOR_USER_NAME = 64;
        public static final int MIN_SIZE_FOR_PASSWORD = 6;
        public static final int MAX_SIZE_FOR_PASSWORD = 255;
        public static final int MIN_SIZE_FOR_NAME = 2;
        public static final int MAX_SIZE_FOR_NAME = 20;
        public static final int MIN_SIZE_FOR_EMAIL = 6;
        public static final int MAX_SIZE_FOR_EMAIL = 100;
    }

    public class Marathon {
        public static final int MAX_SIZE_FOR_DESCRIPTION_OF_MARATHON = 2000;
    }

    public class Schedule {
        public static final String DELETE_CODE_CONFIRMATION = "delete from ConfirmationCode where now()> expired";
    }
}
