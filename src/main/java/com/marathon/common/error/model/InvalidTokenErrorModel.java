package com.marathon.common.error.model;

public class InvalidTokenErrorModel extends ErrorModel {
    public InvalidTokenErrorModel() {
        super("token.not.valid", "Token is not valid");
    }
}
