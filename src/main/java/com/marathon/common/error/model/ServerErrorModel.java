package com.marathon.common.error.model;

public class ServerErrorModel extends ErrorModel {

    public ServerErrorModel(String code, String message) {
        super(code, message);
    }
}
