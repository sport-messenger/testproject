package com.marathon.common.mapper;

import com.marathon.model.dto.request.UpdatePersonalDataRequest;
import com.marathon.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public User map(User user, UpdatePersonalDataRequest request) {
        user.setUsername(request.getUsername() == null ? user.getUsername() : request.getUsername());
        user.setFirstName(request.getFirstName() == null ? user.getFirstName() : request.getFirstName());
        user.setLastName(request.getLastName() == null ? user.getLastName() : request.getLastName());
        user.setHeight(request.getHeight() == 0 ? user.getHeight() : request.getHeight());
        user.setWeight(request.getWeight() == 0 ? user.getWeight() : request.getWeight());
        user.setAge(request.getAge() == null ? user.getAge() : request.getAge());
        user.setGender(request.getGender() == null ? user.getGender() : request.getGender());
        return user;
    }
}
