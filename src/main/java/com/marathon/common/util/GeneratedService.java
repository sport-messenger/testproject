package com.marathon.common.util;

import java.util.Date;
import java.util.Random;

public class GeneratedService {

    private static final int SIX_DIGIT_CODE = 6;
    private static final int RANGE = 10;

    public static int getSixDigitCode() {
        StringBuilder value = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < SIX_DIGIT_CODE; i++) {
            value.append(random.nextInt(RANGE));
        }
        return Integer.parseInt(value.toString());
    }

    public static Date getDateAfterFifteenMin() {
        long currentDateMills = new Date().getTime();
        return new Date(currentDateMills + (15 * 60 * 1000));
    }
}
