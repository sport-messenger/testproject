package com.marathon.common.util;

import com.marathon.common.annotation.property.FileStorageProperties;
import com.marathon.common.exception.FileStorageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileStorageService {

    private final String fileStorageFolder;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties) {
        this.fileStorageFolder = fileStorageProperties.getUploadDir();
    }

    public String storeFile(MultipartFile file, String subFolderPath, String fileName) {
        try {
            String extension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            fileName = fileName + extension;
            String fullDirectoryPath = fileStorageFolder + "/" + subFolderPath;
            File directory = new File(fullDirectoryPath);
            if (!directory.exists()) {
                directory.mkdirs();
            }

            Path fileStorageLocation = Paths.get(fullDirectoryPath).toAbsolutePath().normalize();
            Path targetLocation = fileStorageLocation.resolve(fileName);

            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return fullDirectoryPath.replace(".", "") + "/" + fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }
}
