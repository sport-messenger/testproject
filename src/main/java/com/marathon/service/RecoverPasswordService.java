package com.marathon.service;

import com.marathon.model.User;

public interface RecoverPasswordService {

    void createResetKey(User user);

    boolean checkResetKey(User user, int resetKey);

    void recoverPassword(User user, String password, String confirmPassword);
}
