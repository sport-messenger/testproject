package com.marathon.service;

import com.marathon.model.User;
import com.marathon.model.dto.request.RegistrationRequest;
import com.marathon.model.dto.request.UpdatePersonalDataRequest;
import com.marathon.model.dto.response.RegistrationResponse;

import java.text.ParseException;
import java.util.List;

public interface UserService {

    User save(User user);

    List<User> getAll();

    User findByUsername(String username);

    User findByEmail(String email);

    User findById(Long id);

    void deleteById(Long id);

    User findByUsernameOrEmail(String username, String email);

    RegistrationResponse userRegistration(RegistrationRequest registrationRequest) throws ParseException;

    User updatePersonalData(String username, UpdatePersonalDataRequest request);

    void updatePassword(User user, String currentPassword, String password, String confirmPassword);

    void updateEmail(String email, String password, User user);

    void updateUsername(String username, User user);
}
