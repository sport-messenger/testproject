package com.marathon.service.impl;

import com.marathon.common.util.GeneratedService;
import com.marathon.exeption.CodeConfirmationException;
import com.marathon.exeption.UserException;
import com.marathon.jms.Producer;
import com.marathon.model.Email;
import com.marathon.model.User;
import com.marathon.model.enumeration.error.CodeConfirmationErrorModel;
import com.marathon.model.enumeration.error.UpdatePasswordErrorModel;
import com.marathon.repository.UserRepository;
import com.marathon.service.RecoverPasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class RecoverPasswordServiceImpl implements RecoverPasswordService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private Producer producer;

    @Override
    public void createResetKey(User user) {
        user.setResetKey(GeneratedService.getSixDigitCode());
        user.setUpdated(new Date());
        userRepository.save(user);
        producer.sendMessage(
                "reset-key",
                new Email("aleksey.tulei@gmail.com", user.getEmail(), "Reset key", String.valueOf(user.getResetKey())));
    }

    @Override
    public boolean checkResetKey(User user, int resetKey) {
        if (user.getResetKey() == resetKey) {
            return true;
        }
        //TODO use common exception instead specific
        throw new CodeConfirmationException(HttpStatus.UNPROCESSABLE_ENTITY, new CodeConfirmationErrorModel());
    }

    @Override
    public void recoverPassword(User user, String password, String confirmPassword) {
        if (password.equals(confirmPassword)) {
            user.setPassword(passwordEncoder.encode(password));
            user.setUpdated(new Date());
            userRepository.save(user);
            producer.sendMessage(
                    "congratulation",
                    new Email("aleksey.tulei@gmail.com", user.getEmail(), "Congratulations!", "Password was successfully changed."));
        } else {
            //TODO use common exception instead specific
            throw new UserException(HttpStatus.UNPROCESSABLE_ENTITY, new UpdatePasswordErrorModel());
        }
    }
}
