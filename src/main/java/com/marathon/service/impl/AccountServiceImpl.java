package com.marathon.service.impl;

import com.marathon.exeption.UserException;
import com.marathon.model.ConfirmationCode;
import com.marathon.model.User;
import com.marathon.model.enumeration.UserStatus;
import com.marathon.model.enumeration.error.ActivatedCodeErrorModel;
import com.marathon.model.enumeration.error.UserAlreadyActivatedErrorModel;
import com.marathon.service.AccountService;
import com.marathon.service.ConfirmationCodeService;
import com.marathon.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private UserService userService;
    @Autowired
    private ConfirmationCodeService confirmationCodeService;

    @Override
    @Transactional
    public User activateAccount(int code, User user) {
        if (user.getStatus().equals(UserStatus.ACTIVE)) {
            throw new UserException(HttpStatus.BAD_REQUEST, new UserAlreadyActivatedErrorModel());
        }
        ConfirmationCode confirmationCode = confirmationCodeService.getByUser(user);
        if (code != confirmationCode.getCode()) {
            throw new UserException(HttpStatus.BAD_REQUEST, new ActivatedCodeErrorModel());
        }
        user.setStatus(UserStatus.ACTIVE);
        userService.save(user);
        confirmationCodeService.deleteByUserId(user.getId());
        return user;
    }
}
