package com.marathon.service.impl;

import com.marathon.common.util.GeneratedService;
import com.marathon.exeption.CodeConfirmationException;
import com.marathon.jms.Producer;
import com.marathon.model.ConfirmationCode;
import com.marathon.model.Email;
import com.marathon.model.User;
import com.marathon.model.enumeration.error.CodeConfirmationErrorModel;
import com.marathon.repository.ConfirmationCodeRepository;
import com.marathon.service.ConfirmationCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ConfirmationCodeServiceImpl implements ConfirmationCodeService {

    @Autowired
    private ConfirmationCodeRepository confirmationCodeRepository;
    @Autowired
    private Producer producer;

    @Override
    public ConfirmationCode save(ConfirmationCode code) {
        code.setCreated(new Date());
        code.setUpdated(new Date());
        return confirmationCodeRepository.save(code);
    }

    @Override
    public ConfirmationCode getByUser(User user) {
        ConfirmationCode confirmationCode = confirmationCodeRepository.findByUser(user);
        if (confirmationCode == null) {
            throw new CodeConfirmationException(HttpStatus.BAD_REQUEST, new CodeConfirmationErrorModel());
        }
        return confirmationCode;
    }

    @Override
    public void deleteByUserId(Long userId) {
        confirmationCodeRepository.deleteByUserId(userId);
    }

    @Override
    public void resetCode(User user) {
        ConfirmationCode code = confirmationCodeRepository.findByUser(user);
        if (code != null) {
            code.setCode(GeneratedService.getSixDigitCode());
            code.setUpdated(new Date());
            code.setExpired(GeneratedService.getDateAfterFifteenMin());
            confirmationCodeRepository.save(code);
        } else {
            code = new ConfirmationCode(GeneratedService.getSixDigitCode(), user, GeneratedService.getDateAfterFifteenMin());
            this.save(code);
        }
        producer.sendMessage("reset-code", new Email("aleksey.tulei@gmail.com", user.getEmail(), "resetCode", String.valueOf(code.getCode())));
    }
}
