package com.marathon.service.impl;

import com.marathon.common.mapper.UserMapper;
import com.marathon.common.util.GeneratedService;
import com.marathon.exeption.UserException;
import com.marathon.jms.Producer;
import com.marathon.model.ConfirmationCode;
import com.marathon.model.Email;
import com.marathon.model.Role;
import com.marathon.model.User;
import com.marathon.model.dto.request.RegistrationRequest;
import com.marathon.model.dto.request.UpdatePersonalDataRequest;
import com.marathon.model.dto.response.RegistrationResponse;
import com.marathon.model.enumeration.UserStatus;
import com.marathon.model.enumeration.error.EmailAlreadyExistErrorModel;
import com.marathon.model.enumeration.error.UpdatePasswordErrorModel;
import com.marathon.model.enumeration.error.UpdateUsernameErrorModel;
import com.marathon.repository.RoleRepository;
import com.marathon.repository.UserRepository;
import com.marathon.repository.jpa.UserJpaRepository;
import com.marathon.security.jwt.JwtTokenProvider;
import com.marathon.service.ConfirmationCodeService;
import com.marathon.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserJpaRepository userJpaRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private ConfirmationCodeService confirmationCodeService;
    @Autowired
    private Producer producer;

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> getAll() {
        List<User> users = userRepository.getAll();
        LOGGER.info("IN getAll method - {} users found", users.size());
        return users;
    }

    @Override
    public User findByUsername(String username) {
        User result = userRepository.getByUserName(username);
        LOGGER.info("IN findByUsername method - user: {} found by username: {}", result, result.getUsername());
        return result;
    }

    @Override
    public User findById(Long id) {
        User result = userRepository.get(id);
        LOGGER.info("IN findById method - user: {} found by id: ", result);
        return result;
    }

    @Override
    public void deleteById(Long id) {
        userRepository.delete(id);
        LOGGER.info("IN deleteById method - user with id: {} successfully deleted", id);
    }

    @Override
    public User updatePersonalData(String username, UpdatePersonalDataRequest request) {
        User user = findByUsername(username);
        return userRepository.save(userMapper.map(user, request));
    }

    @Override
    public User findByEmail(String email) {
        User user = userRepository.findByEmail(email);
        return user;
    }

    @Override
    public User findByUsernameOrEmail(String username, String email) {
        User user = userRepository.findByUsernameOrEmail(username, email);
        return user;
    }

    @Override
    @Transactional
    public RegistrationResponse userRegistration(RegistrationRequest registrationRequest) throws ParseException {
        User user = save(registrationRequest);
        ConfirmationCode code = new ConfirmationCode(GeneratedService.getSixDigitCode(), user, GeneratedService.getDateAfterFifteenMin());
        ConfirmationCode confirmCode = confirmationCodeService.save(code);
        String token = jwtTokenProvider.createToken(user.getUsername(), user.getRoles());
        producer.sendMessage("registration",
                new Email("aleksey.tulei@gmail.com", user.getEmail(), "Confirm registration", String.valueOf(confirmCode.getCode())));
        return new RegistrationResponse(user, token);
    }

    @Override
    public void updatePassword(User user, String currentPassword, String password, String confirmPassword) {
        if (passwordEncoder.matches(currentPassword, user.getPassword()) && password.equals(confirmPassword)) {
            user.setPassword(passwordEncoder.encode(password));
            user.setUpdated(new Date());
            userRepository.save(user);
        }
        throw new UserException(HttpStatus.UNPROCESSABLE_ENTITY, new UpdatePasswordErrorModel());
    }

    private User save(RegistrationRequest request) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(
                "yyyy/MM/dd");
        User user = new User();
        Role roleUser = roleRepository.findByName("USER");
        List<Role> userRoles = new ArrayList<>();
        userRoles.add(roleUser);
        user.setUsername(request.getUsername());
        user.setPassword(passwordEncoder.encode(request.getConfirmPassword()));
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setEmail(request.getEmail());
        user.setAvatar(request.getAvatar());
        user.setHeight(request.getHeight());
        user.setWeight(request.getWeight());
        user.setAge(formatter.parse(formatter.format(request.getAge())));
        user.setGender(request.getGender());
        user.setStatus(UserStatus.NOT_ACTIVE);
        user.setCreated(new Date());
        user.setUpdated(new Date());
        user.setRoles(userRoles);
        user = userRepository.save(user);
        LOGGER.info("IN register method - user: {} successfully registered", user);
        return user;
    }

    @Override
    public void updateEmail(String email, String password, User user) {
        if (passwordEncoder.matches(user.getPassword(), password)) {
            if (userRepository.findByEmail(email) != null) {
                user.setEmail(email);
                user.setUpdated(new Date());
                userRepository.save(user);
            }
            throw new UserException(HttpStatus.UNPROCESSABLE_ENTITY, new EmailAlreadyExistErrorModel());
        }
        throw new UserException(HttpStatus.UNPROCESSABLE_ENTITY, new UpdatePasswordErrorModel());
    }


    @Override
    public void updateUsername(String username, User user) {
        if (userRepository.getByUserName(username) != null) {
            user.setUsername(username);
            user.setUpdated(new Date());
            userRepository.save(user);
        }
        throw new UserException(HttpStatus.UNPROCESSABLE_ENTITY, new UpdateUsernameErrorModel());
    }
}
