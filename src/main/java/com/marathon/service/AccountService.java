package com.marathon.service;

import com.marathon.model.User;

public interface AccountService {
    User activateAccount(int code, User user);
}
