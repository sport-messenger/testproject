package com.marathon.service;

import com.marathon.model.ConfirmationCode;
import com.marathon.model.User;

public interface ConfirmationCodeService {

    ConfirmationCode save(ConfirmationCode code);

    ConfirmationCode getByUser(User user);

    void deleteByUserId(Long userId);

    void resetCode(User user);
}
