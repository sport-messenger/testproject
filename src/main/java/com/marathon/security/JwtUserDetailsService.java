package com.marathon.security;

import com.marathon.model.User;
import com.marathon.security.jwt.JwtUser;
import com.marathon.security.jwt.JwtUserFactory;
import com.marathon.service.UserService;
import com.marathon.service.impl.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User with username: " + username + " not found.");
        }
        JwtUser jwtUser = JwtUserFactory.create(user);
        LOGGER.info("IN loadByUserName - user with username: {} successfully loaded", username);
        return jwtUser;
    }
}
