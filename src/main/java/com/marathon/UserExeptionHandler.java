package com.marathon;

import com.marathon.common.error.model.ErrorModel;
import com.marathon.common.error.model.ServerErrorModel;
import com.marathon.exeption.CodeConfirmationException;
import com.marathon.exeption.UserException;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.MediaType.APPLICATION_JSON;

@RestControllerAdvice
public class UserExeptionHandler {

    private static final Logger LOGGER = Logger.getLogger(UserExeptionHandler.class);

    @ExceptionHandler(UserException.class)
    public ResponseEntity handleUserException(UserException ex) {
        LOGGER.error(ex);
        return createResponseEntity(ex.getStatus(), ex.getErrorModel());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity handleException(Exception ex) {
        LOGGER.error(ex);
        return createResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR,
                new ServerErrorModel("internal.server.error", ex.getMessage()));
    }

    @ExceptionHandler(CodeConfirmationException.class)
    public ResponseEntity handleCodeConfirmationException(CodeConfirmationException ex) {
        LOGGER.error(ex);
        return createResponseEntity(ex.getStatus(), ex.getErrorModel());
    }

    private ResponseEntity createResponseEntity(HttpStatus httpStatus, ErrorModel errorModel) {
        return ResponseEntity
                .status(httpStatus)
                .contentType(APPLICATION_JSON)
                .body(errorModel);
    }
}
