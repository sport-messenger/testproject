package com.marathon.exeption;


import com.marathon.common.error.model.ErrorModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserException extends RuntimeException {

    private HttpStatus status;
    private ErrorModel errorModel;

    public UserException(HttpStatus status, ErrorModel errorModel, Throwable throwable) {
        super(throwable);
        this.status = status;
        this.errorModel = errorModel;
    }
}
