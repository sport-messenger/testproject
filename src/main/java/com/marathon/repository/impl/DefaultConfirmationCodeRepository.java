package com.marathon.repository.impl;

import com.marathon.model.ConfirmationCode;
import com.marathon.model.User;
import com.marathon.repository.ConfirmationCodeRepository;
import com.marathon.repository.jpa.ConfirmationCodeJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

@Repository
@Profile({"prod", "default"})
public class DefaultConfirmationCodeRepository implements ConfirmationCodeRepository {

    @Autowired
    private ConfirmationCodeJpaRepository confirmationCodeJpaRepository;

    @Override
    public ConfirmationCode findByUser(User user) {
        return confirmationCodeJpaRepository.findByUser(user).orElse(null);
    }

    @Override
    public void deleteByUserId(Long id) {
        confirmationCodeJpaRepository.deleteByUserId(id);
    }

    @Override
    public ConfirmationCode save(ConfirmationCode confirmationCode) {
        return confirmationCodeJpaRepository.save(confirmationCode);
    }
}
