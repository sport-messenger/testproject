package com.marathon.repository.impl;

import com.marathon.exeption.UserException;
import com.marathon.model.User;
import com.marathon.model.enumeration.error.UserNotFoundErrorModel;
import com.marathon.repository.jpa.UserJpaRepository;
import com.marathon.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@Profile({"prod", "default"})
public class DefaultUserRepository implements UserRepository {

    @Autowired
    private UserJpaRepository userJpaRepository;

    @Override
    public User get(long id) {
        return userJpaRepository.findById(id).orElseThrow(() -> new UserException(HttpStatus.BAD_REQUEST, new UserNotFoundErrorModel()));
    }

    @Override
    public void delete(long id) {
        userJpaRepository.deleteById(id);
    }

    @Override
    public User save(User user) {
        return userJpaRepository.save(user);
    }

    @Override
    public List<User> getAll() {
        return userJpaRepository.findAll();
    }

    @Override
    public User getByUserName(String userName) {
        Optional<User> optionalUser = userJpaRepository.findByUsername(userName);
        return optionalUser.orElseThrow(() -> new UserException(HttpStatus.BAD_REQUEST, new UserNotFoundErrorModel()));
    }

    @Override
    public User findByEmail(String email) {
        return userJpaRepository.findByEmail(email)
                .orElseThrow(() -> new UserException(HttpStatus.BAD_REQUEST, new UserNotFoundErrorModel()));
    }

    @Override
    public User findByUsernameOrEmail(String username, String email) {
        return userJpaRepository.findByUsernameOrEmail(username, email).orElse(null);
    }
}
