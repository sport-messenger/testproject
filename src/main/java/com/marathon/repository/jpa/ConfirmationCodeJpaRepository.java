package com.marathon.repository.jpa;

import com.marathon.model.ConfirmationCode;
import com.marathon.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConfirmationCodeJpaRepository extends JpaRepository<ConfirmationCode, Long> {

    Optional<ConfirmationCode> findByUser(User user);

    ConfirmationCode getByUser(User user);

    void deleteByUserId(Long id);

}
