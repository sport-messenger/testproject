package com.marathon.repository;

import com.marathon.model.User;

import java.util.List;

public interface UserRepository {

    User get(long id);

    void delete(long id);

    User save(User user);

    List<User> getAll();

    User getByUserName(String userName);

    User findByEmail(String email);

    User findByUsernameOrEmail(String username, String email);
}
