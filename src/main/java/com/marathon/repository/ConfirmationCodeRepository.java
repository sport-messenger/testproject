package com.marathon.repository;

import com.marathon.model.ConfirmationCode;
import com.marathon.model.User;

public interface ConfirmationCodeRepository {

    ConfirmationCode findByUser(User user);

    void deleteByUserId(Long id);

    ConfirmationCode save(ConfirmationCode confirmationCode);
}
