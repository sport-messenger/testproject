package com.marathon.user.repository;

import com.marathon.model.User;
import com.marathon.repository.UserRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
@Profile("test")
public class StubUserRepository implements UserRepository {

    private List<User> users = new ArrayList<>();

    @Override
    public User get(long id) {
        return users.stream()
                .filter(user -> user.getId().equals(id))
                .findAny()
                .orElse(null);
    }

    @Override
    public void delete(long id) {
        users.removeIf(user -> user.getId().equals(id));
    }

    @Override
    public User save(User user) {
        users.stream()
                .filter(u -> u.getId().equals(user.getId()))
                .findFirst()
                .ifPresent(userFromDb -> users.remove(userFromDb));
        users.add(user);
        return user;
    }

    @Override
    public List<User> getAll() {
        return users;
    }

    @Override
    public User getByUserName(String userName) {
        return users.stream()
                .filter(user -> user.getUsername().equals(userName))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByEmail(String email) {
        return users.stream()
                .filter(user -> user.getEmail().equals(email))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByUsernameOrEmail(String username, String email) {
        return users.stream()
                .filter(user -> user.getUsername().equals(username) || user.getEmail().equals(email))
                .findFirst()
                .orElse(null);
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
