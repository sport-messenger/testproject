package com.marathon.user.repository;

import com.marathon.model.ConfirmationCode;
import com.marathon.model.User;
import com.marathon.repository.ConfirmationCodeRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
@Profile("test")
public class StubConfirmationCodeRepository implements ConfirmationCodeRepository {

    private List<ConfirmationCode> confirmationCode = new ArrayList<>();

    @Override
    public ConfirmationCode findByUser(User user) {
        return confirmationCode.stream()
                .filter(code -> code.getUser().equals(user))
                .findAny()
                .orElse(null);
    }

    @Override
    public void deleteByUserId(Long id) {
        confirmationCode.removeIf(code -> code.getUser().getId().equals(id));
    }

    @Override
    public ConfirmationCode save(ConfirmationCode code) {
        confirmationCode.stream()
                .filter(c -> c.getId().equals(code.getId()))
                .findFirst()
                .ifPresent(codeFromDb -> confirmationCode.remove(codeFromDb));
        confirmationCode.add(code);
        return code;
    }

    public List<ConfirmationCode> getConfirmationCode() {
        return confirmationCode;
    }

    public void setConfirmationCode(List<ConfirmationCode> confirmationCode) {
        this.confirmationCode = confirmationCode;
    }
}
