package com.marathon.user.mapper

import com.marathon.common.mapper.UserMapper
import com.marathon.model.User
import com.marathon.model.dto.request.UpdatePersonalDataRequest
import com.marathon.model.enumeration.Gender
import com.marathon.model.enumeration.UserStatus
import spock.lang.Specification
import spock.lang.Subject

class UserMapperTest extends Specification {

    @Subject
    def userMapper = new UserMapper()


    def "should map UpdatePersonalData to User"() {
        given:
        def user = new User("userName", "password", "firstName", "lastName", "email", "avatar", 180, 100,
                new Date(1997, 03, 12), Gender.MALE, UserStatus.ACTIVE,null, null, null)
        def userWithNewData = new UpdatePersonalDataRequest()
        userWithNewData.height = 200

        when:
        def userWithUpdatedDate = userMapper.map(user, userWithNewData)

        then:
        with(userWithUpdatedDate){
            username == "userName"
            password == "password"
            firstName == "firstName"
            lastName == "lastName"
            email == "email"
            avatar == "avatar"
            weight == 100
            height == 200
            age == new Date(1997, 03, 12)
            gender == Gender.MALE
            status == UserStatus.ACTIVE
            roles == null
            marathons == null
        }
    }
}
