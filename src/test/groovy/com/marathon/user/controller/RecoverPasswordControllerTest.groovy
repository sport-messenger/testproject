package com.marathon.user.controller

import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import com.google.gson.Gson
import com.marathon.MarathonApplication
import com.marathon.model.User
import com.marathon.model.dto.request.ConfirmationCodeRequest
import com.marathon.model.dto.request.UpdatePasswordRequest
import com.marathon.model.enumeration.UserStatus
import com.marathon.service.impl.RecoverPasswordServiceImpl
import com.marathon.service.impl.UserServiceImpl
import com.marathon.user.repository.StubUserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@ComponentScan("com.marathon")
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = [MarathonApplication]
)
@ActiveProfiles("test")
class RecoverPasswordControllerTest extends Specification {

    private Gson gson = new Gson()
    @Autowired
    private MockMvc mockMvc
    @Subject
    @Autowired
    private UserServiceImpl userService
    @Subject
    @Autowired
    private RecoverPasswordServiceImpl recoverPasswordService;
    @Autowired
    private StubUserRepository stubUserRepository

    void setup(){
        stubUserRepository.users.clear()
    }

    // TODO mock mailService so that don't send real message
    def "when post (/api/v1/recover-passwords) then return status 200 and create reset key"() {
        given:
        def users = [new User("userName", "password", "firstName", "lastName", "test@test.com", "avatar", 100, 60, null, null, UserStatus.NOT_ACTIVE, null, null, null)]
        users.get(0).setId(123)
        stubUserRepository.setUsers(users)

        def confirmationCodeRequest = new ConfirmationCodeRequest("test@test.com",null)
        def request = gson.toJson(confirmationCodeRequest, ConfirmationCodeRequest.class)

        when:
        def result = mockMvc.perform(post("/api/v1/recover-passwords")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))

        then:
        result.andExpect(status().isOk())

        and:
        result.andExpect(jsonPath('$.success').value(true))
        result.andExpect(jsonPath('$.message').value("Reset code was created"))
    }

    def "when patch (/api/v1/recover-passwords) then return status 200 and update password"() {
        given:
        def users = [new User("userName", "password", "firstName", "lastName", "test@test.com", "avatar", 100, 60, null, null, UserStatus.NOT_ACTIVE, null, null, null)]
        users.get(0).setId(123)
        stubUserRepository.setUsers(users)

        def updatePasswordRequest = new UpdatePasswordRequest("test@test.com","123123","321321","321321")
        def request = gson.toJson(updatePasswordRequest, UpdatePasswordRequest.class)

        when:
        def result = mockMvc.perform(patch("/api/v1/recover-passwords")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))

        then:
        result.andExpect(status().isOk())

        and:
        result.andExpect(jsonPath('$.success').value(true))
        result.andExpect(jsonPath('$.message').value("Password changed"))
    }

    def "when patch (/api/v1/recover-passwords) with not equals password then return status 422" (){
        given:
        def users = [new User("userName", "password", "firstName", "lastName", "test@test.com", "avatar", 100, 60, null, null, UserStatus.NOT_ACTIVE, null, null, null)]
        users.get(0).setId(123)
        stubUserRepository.setUsers(users)

        def updatePasswordRequest = new UpdatePasswordRequest("test@test.com","password","321321","123123")
        def request = gson.toJson(updatePasswordRequest, UpdatePasswordRequest.class)

        when:
        def result = mockMvc.perform(patch("/api/v1/recover-passwords")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))

        then:
        result.andExpect(status().isUnprocessableEntity())
    }

    def "when post (/api/v1/recover-passwords/check) with valid data the return status 200"(){
        given:
        def users = [new User("userName", "password", "firstName", "lastName", "test@test.com", "avatar", 100, 60, null, null, UserStatus.NOT_ACTIVE, 123456, null, null)]
        users.get(0).setId(123)
        stubUserRepository.setUsers(users)

        def confirmationCodeRequest = new ConfirmationCodeRequest("test@test.com","123456")
        def request = gson.toJson(confirmationCodeRequest, ConfirmationCodeRequest.class)

        when:
        def result = mockMvc.perform(post("/api/v1/recover-passwords/check")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))

        then:
        result.andExpect(status().isOk())

        and:
        result.andExpect(jsonPath('$.success').value(true))
        result.andExpect(jsonPath('$.message').value("Reset key confirmed"))
    }

    def "when post (/api/v1/recover-passwords/{reset-key}/check) then return status 422"() {
        given:
        def users = [new User("userName", "password", "firstName", "lastName", "test@test.com", "avatar", 100, 60, null, null, UserStatus.NOT_ACTIVE, 123456, null, null)]
        users.get(0).setId(123)
        stubUserRepository.setUsers(users)

        def confirmationCodeRequest = new ConfirmationCodeRequest("test@test.com","654321")
        def request = gson.toJson(confirmationCodeRequest, ConfirmationCodeRequest.class)

        when:
        def result = mockMvc.perform(post("/api/v1/recover-passwords/check")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))

        then:
        result.andExpect(status().isUnprocessableEntity())
    }
}
