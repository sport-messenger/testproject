package com.marathon.user.controller


import com.marathon.MarathonApplication
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification

@ComponentScan("com.marathon")
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = [MarathonApplication]
)
@ActiveProfiles("test")
class AuthenticationControllerTest extends Specification {

}
