package com.marathon.user.controller

import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import com.marathon.MarathonApplication
import com.marathon.model.User
import com.marathon.service.impl.ConfirmationCodeServiceImpl
import com.marathon.user.repository.StubConfirmationCodeRepository
import com.marathon.user.repository.StubUserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@ComponentScan("com.marathon")
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = [MarathonApplication]
)
@ActiveProfiles("test")
class ConfirmationCodeControllerTest extends Specification {

    @Autowired
    private MockMvc mockMvc
    @Subject
    @Autowired
    private ConfirmationCodeServiceImpl confirmationCodeService
    @Autowired
    private StubConfirmationCodeRepository stubConfirmationCodeRepository
    @Autowired
    private StubUserRepository stubUserRepository

    void setup() {
        stubUserRepository.users.clear()
        stubConfirmationCodeRepository.confirmationCode.clear()
    }

    def "when PUT (/api/v1/confirmation-codes) then return 200 status and success message"(){
        given:
        def users = [new User("userName", "password", "firstName", "lastName", "email", "avatar", 100, 60, null, null, null, null, null, null)]
        users.get(0).setId(123)
        stubUserRepository.setUsers(users)

        when:
        def result = mockMvc.perform(put("/api/v1/confirmation-codes/123")
                .contentType(MediaType.APPLICATION_JSON))

        then:
        result.andExpect(status().isOk())
    }
}
