package com.marathon.user.controller

import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import com.google.gson.Gson
import com.marathon.MarathonApplication
import com.marathon.common.mapper.UserMapper
import com.marathon.model.User
import com.marathon.model.dto.request.UpdatePasswordRequest
import com.marathon.model.dto.request.UpdatePersonalDataRequest
import com.marathon.service.impl.UserServiceImpl
import com.marathon.user.repository.StubUserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.http.MediaType
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@ComponentScan("com.marathon")
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = [MarathonApplication]
)
@ActiveProfiles("test")
class UserControllerTest extends Specification {
    private static final String USER_ID = "123"
    private Gson gson = new Gson()
    @Autowired
    private MockMvc mockMvc
    @Subject
    @Autowired
    private UserServiceImpl userService
    @Subject
    @Autowired
    private UserMapper userMapper
    @Autowired
    private StubUserRepository stubUserRepository
    @Autowired
    private BCryptPasswordEncoder passwordEncoder

    void setup() {
        stubUserRepository.users.clear()
    }

    def "when PATCH (/api/v1/users/{userId}) then return status code 200 after success update"() {
        given:
        def users = [new User("userName", "password", "firstName", "lastName", "email", "avatar", 100, 60, null, null, null, null, null, null)]
        users.get(0).setId(123)
        stubUserRepository.setUsers(users)

        def userWithNewData = new UpdatePersonalDataRequest()
        userWithNewData.username = "newUsername"
        userWithNewData.height = 200
        def request = gson.toJson(userWithNewData, UpdatePersonalDataRequest.class)

        when:
        def result = mockMvc.perform(patch("/api/v1/users/$USER_ID")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))

        then:
        result.andExpect(status().isOk())
                .andReturn()

        and:
        result.andExpect(jsonPath('$.id').value(123))
        result.andExpect(jsonPath('$.username').value("newUsername"))
        result.andExpect(jsonPath('$.firstName').value("firstName"))
        result.andExpect(jsonPath('$.lastName').value("lastName"))
        result.andExpect(jsonPath('$.height').value(200))
        result.andExpect(jsonPath('$.weight').value(60))
    }

    def "when GET (/api/v1/users/{userId}) then return user by id and status code 200"() {
        given:
        def users = [new User("userName", "password", "firstName", "lastName", "email", "avatar", 100, 60, null, null, null, null, null, null)]
        users.get(0).setId(123)
        stubUserRepository.setUsers(users)

        when:
        def result = mockMvc.perform(get("/api/v1/users/$USER_ID")
                .contentType(MediaType.APPLICATION_JSON))

        then:
        result.andExpect(status().isOk())
                .andReturn()
        and:
        result.andExpect(jsonPath('$.id').value(123))
        result.andExpect(jsonPath('$.username').value("userName"))
        result.andExpect(jsonPath('$.firstName').value("firstName"))
        result.andExpect(jsonPath('$.lastName').value("lastName"))
        result.andExpect(jsonPath('$.email').value("email"))
        result.andExpect(jsonPath('$.avatar').value("avatar"))
        result.andExpect(jsonPath('$.height').value(100))
        result.andExpect(jsonPath('$.weight').value(60))
    }

    def "when PATCH (/api/v1/users/{userId}/update-password) then return user with updated password and status code 200"() {
        given:
        def users = [new User("userName", passwordEncoder.encode("password"), "firstName", "lastName", "email", "avatar", 100, 60, null, null, null, null, null, null)]
        users.get(0).setId(123)
        stubUserRepository.setUsers(users)

        def updatePasswordRequest = new UpdatePasswordRequest(null, "password", "222", "222")
        def request = gson.toJson(updatePasswordRequest, UpdatePasswordRequest.class)

        when:
        def result = mockMvc.perform(patch("/api/v1/users/$USER_ID/update-password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))

        then:
        result.andExpect(status().isOk())
                .andReturn()

        and:
        result.andExpect(jsonPath('$.success').value(true))
        result.andExpect(jsonPath('$.message').value("Password changed"))
    }

    def "when PATCH (/api/v1/users/{userId}/update-password) then return status code 422 if new password don't equal confirmed password"() {
        given:
        def users = [new User("userName", passwordEncoder.encode("password"), "firstName", "lastName", "email", "avatar", 100, 60, null, null, null, null, null, null)]
        users.get(0).setId(123)
        stubUserRepository.setUsers(users)

        def updatePasswordRequest = new UpdatePasswordRequest(null, "password", "333", "222")
        def request = gson.toJson(updatePasswordRequest, UpdatePasswordRequest.class)

        when:
        def result = mockMvc.perform(patch("/api/v1/users/$USER_ID/update-password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))

        then:
        result.andExpect(status().isUnprocessableEntity())
                .andReturn()

        and:
        result.andExpect(jsonPath('$.code').value("1000.0"))
        result.andExpect(jsonPath('$.message').value("Password mismatch"))
    }

    def "when PATCH (/api/v1/users/{userId}/update-password) then return status code 422 if password don't equal current"() {
        given:
        def users = [new User("userName", passwordEncoder.encode("password"), "firstName", "lastName", "email", "avatar", 100, 60, null, null, null, null, null, null)]
        users.get(0).setId(123)
        stubUserRepository.setUsers(users)

        def updatePasswordRequest = new UpdatePasswordRequest(null, "123", "333", "333")
        def request = gson.toJson(updatePasswordRequest, UpdatePasswordRequest.class)

        when:
        def result = mockMvc.perform(patch("/api/v1/users/$USER_ID/update-password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))

        then:
        result.andExpect(status().isUnprocessableEntity())
                .andReturn()

        and:
        result.andExpect(jsonPath('$.code').value("1000.0"))
        result.andExpect(jsonPath('$.message').value("Password mismatch"))
    }
}
