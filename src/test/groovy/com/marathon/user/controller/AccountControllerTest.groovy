package com.marathon.user.controller

import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import com.marathon.MarathonApplication
import com.marathon.model.ConfirmationCode
import com.marathon.model.User
import com.marathon.model.enumeration.UserStatus
import com.marathon.service.impl.ConfirmationCodeServiceImpl
import com.marathon.service.impl.UserServiceImpl
import com.marathon.user.repository.StubConfirmationCodeRepository
import com.marathon.user.repository.StubUserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@ComponentScan("com.marathon")
@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = [MarathonApplication]
)
@ActiveProfiles("test")
class AccountControllerTest extends Specification {

    @Autowired
    private MockMvc mockMvc
    @Subject
    @Autowired
    private UserServiceImpl userService
    @Subject
    @Autowired
    private ConfirmationCodeServiceImpl confirmationCodeService
    @Autowired
    private StubConfirmationCodeRepository stubConfirmationCodeRepository
    @Autowired
    private StubUserRepository stubUserRepository

    void setup(){
        stubConfirmationCodeRepository.confirmationCode.clear()
        stubUserRepository.users.clear()
    }

    def "when POST (/api/v1/accounts/{userId}) the return status 200 and updated user"() {
        given:
        def users = [new User("userName", "password", "firstName", "lastName", "email", "avatar", 100, 60, null, null, UserStatus.NOT_ACTIVE, null, null, null)]
        users.get(0).setId(123)
        stubUserRepository.setUsers(users)
        stubConfirmationCodeRepository.setConfirmationCode([new ConfirmationCode(123456, users.get(0), new Date())])

        when:
        def result = mockMvc.perform(post("/api/v1//accounts/123?code=123456")
                .contentType(MediaType.APPLICATION_JSON))

        then:
        result.andExpect(status().isOk())

        and:
        result.andExpect(jsonPath('$.id').value(123))
        result.andExpect(jsonPath('$.username').value("userName"))
        result.andExpect(jsonPath('$.firstName').value("firstName"))
        result.andExpect(jsonPath('$.lastName').value("lastName"))
        result.andExpect(jsonPath('$.email').value("email"))
        result.andExpect(jsonPath('$.avatar').value("avatar"))
        result.andExpect(jsonPath('$.height').value(100))
        result.andExpect(jsonPath('$.weight').value(60))
        result.andExpect(jsonPath('$.status').value("ACTIVE"))
    }

    def "when POST (/api/v1/accounts/{userId}) the return status 400"() {
        given:
        def users = [new User("userName", "password", "firstName", "lastName", "email", "avatar", 100, 60, null, null, UserStatus.NOT_ACTIVE, null, null, null)]
        users.get(0).setId(123)
        stubUserRepository.setUsers(users)
        stubConfirmationCodeRepository.setConfirmationCode([new ConfirmationCode(123456, users.get(0), new Date())])

        when:
        def result = mockMvc.perform(post("/api/v1/accounts/123?code=321654")
                .contentType(MediaType.APPLICATION_JSON))

        then:
        result.andExpect(status().isBadRequest())
                .andReturn()

        and:
        result.andExpect(jsonPath('$.success').value(false))
        result.andExpect(jsonPath('$.message').value("Bad request!"))
    }
}
